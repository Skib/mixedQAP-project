#ifndef _hillclimber_h
#define _hillclimber_h_

#include <random>
#include <mixedQAPeval.h>
#include <search.h>
#include <uniformContinue.h>
#include <solution.h>

class HillClimber : public Search
{
public:
    HillClimber(MixedQAPeval &_eval) : eval(_eval) {}

    virtual void operator()(Solution &_solution)
    {
        std::pair<unsigned int, unsigned int> bestMove;
        UniformContinue uniform(_solution.x.size());
        Solution s(_solution.sigma.size());
        uniform(s);
        int nbIter = 0;
        while (time(NULL) < timeLimit_)
        {
            bestMove = selectBestNeighbor(_solution);
            // on arrête si on ne trouve pas de meilleur voisin
            if (bestMove.first == bestMove.second)
                break;
            std::swap(_solution.sigma[bestMove.first], _solution.sigma[bestMove.second]);
            nbIter++;
            std::cout << _solution.fitness << std::endl;
        }
        std::cout << "nbIter : " << nbIter << std::endl;
        eval(_solution);
    }

protected:
    virtual std::pair<unsigned int, unsigned int> selectBestNeighbor(Solution &_solution)
    {
        std::pair<unsigned int, unsigned int> select;
        eval(_solution);
        double bestFitness = _solution.fitness;
        select.first = 0;
        select.second = 0;
        // essayer toutes les permutations possibles
        for (unsigned int i = 1; i < _solution.sigma.size(); i++)
        {
            for (unsigned int j = 0; j < i; j++)
            {
                std::swap(_solution.sigma[i], _solution.sigma[j]);
                eval(_solution);
                if (_solution.fitness > bestFitness)
                {
                    select.first = i;
                    select.second = j;
                    bestFitness = _solution.fitness;
                }
                std::swap(_solution.sigma[i], _solution.sigma[j]);
            }
        }
        return select;
    }
    MixedQAPeval &eval;
};

#endif