#ifndef _randomPermutation_h
#define _randomPermutation_h

#include <stdlib.h>  
#include <operator.h>
#include <solution.h>
#include <algorithm>

class RandomPermutation : public Operator {
    public:
        RandomPermutation(std::default_random_engine & _rng, unsigned int _n) : rng(_rng), n(_n) { }

        void operator()(Solution & solution) { 
            solution.sigma.resize(n);
            for (unsigned int k = 0 ; k<n ; k++){
                solution.sigma[k] = k;
            }
            std::shuffle(std::begin(solution.sigma),std::end(solution.sigma),rng);
        }
        
    protected:
        unsigned int n;
        std::default_random_engine rng;
};

#endif